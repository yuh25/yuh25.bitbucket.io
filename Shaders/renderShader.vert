precision highp float;
attribute vec4 aColor;
attribute vec2 aPTextCoord;

uniform sampler2D texture;
uniform mat4 umProjMatrix;
uniform mat4 umViewMatrix;

varying vec4 vColor;

void main(void){
	vec4 pos = texture2D(texture, aPTextCoord);
	gl_Position = umProjMatrix * umViewMatrix * vec4(pos.rgb, 1.0);
	vColor = aColor;
	gl_PointSize = pos.a / 10000.0 * 3.0;
}