function cameraClass(initPos){
	this.moveForward = false;
	this.moveLeft = false;
	this.moveRight = false;
	this.moveBackward = false;
	this.pos = initPos;
	this.yaw = 0.0;
	this.pitch = 0.0;
	this.speed = 1.0;
	this.update = true;
}

cameraClass.prototype.moveCamera = function(){
	var yawInRadians = radians(this.yaw);
	var pitchInRadians = radians(this.pitch);
	if(this.moveForward){
		this.pos[0] -= this.speed * Math.sin(-yawInRadians);
		this.pos[1] -= this.speed * Math.sin(pitchInRadians);
		this.pos[2] -= this.speed * Math.cos(-yawInRadians);
		this.update = true;
	}
	if(this.moveBackward){
		this.pos[0] += this.speed * Math.sin(-yawInRadians);
		this.pos[1] += this.speed * Math.sin(pitchInRadians);
		this.pos[2] += this.speed * Math.cos(-yawInRadians);
		this.update = true;
	}
	if(this.moveRight){
		this.pos[0] += this.speed * Math.cos(yawInRadians);
		this.pos[2] += this.speed * Math.sin(yawInRadians);
		this.update = true;
	}
	if(this.moveLeft){
		this.pos[0] -= this.speed * Math.cos(yawInRadians);
		this.pos[2] -= this.speed * Math.sin(yawInRadians);
		this.update = true;
	}
};

cameraClass.prototype.calcMatrix = function(){
	if(this.update){
		var cx = Math.cos(radians(this.pitch));
		var sx = Math.sin(radians(this.pitch));

		var cy = Math.cos(radians(this.yaw));
		var sy = Math.sin(radians(this.yaw));
		var x = -this.pos[0];
		var y = -this.pos[1];
		var z = -this.pos[2];

		var result =
		[cy,      sx*sy,                   -cx*sy,                 0.0,
		0.0,      cx,                      sx,                     0.0,
		sy,       -sx*cy,                  cx*cy,                  0.0,
		x*cy+z*sy, x*sy*sx+y*cx+(z*cy*-sx), x*-cx*sy+y*sx+z*cx*cy, 1.0];
		this.viewMat = new Float32Array(result);
		this.update = false;
	}
	return this.viewMat;
};

var degToRad = Math.PI / 180.0;
function radians( degrees ) {
	return degrees * degToRad;
}