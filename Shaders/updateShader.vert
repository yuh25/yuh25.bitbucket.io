precision highp float;
attribute vec3 aPosition;
attribute vec2 aTextCoord;

varying vec2 vTextCoord;

void main(void){
	vTextCoord = aTextCoord;
	gl_Position = vec4(aPosition.x*2.0+1.0,aPosition.y*2.0-1.0,0.0,1.0);
}
