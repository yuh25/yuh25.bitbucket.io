function generateTextureBuffer(res){
	var renderTargetTexture = gl.createTexture();
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, renderTargetTexture);

	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, res, res, 0,
											gl.RGBA, gl.FLOAT, null);
	gl.generateMipmap(gl.TEXTURE_2D);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE); 
	var frameBuffer = gl.createFramebuffer();
	gl.bindFramebuffer(gl.FRAMEBUFFER, frameBuffer);

	var renderBuffer = gl.createRenderbuffer();
	gl.bindRenderbuffer(gl.RENDERBUFFER, renderBuffer);
	gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, res, res);

	gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0,
										gl.TEXTURE_2D, renderTargetTexture, 0);
	gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT,
										gl.RENDERBUFFER, renderBuffer);

	var status = gl.checkFramebufferStatus(gl.FRAMEBUFFER);
	if(status != gl.FRAMEBUFFER_COMPLETE){alert('Framebuffer Not Complete');}

	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	gl.bindRenderbuffer(gl.RENDERBUFFER, null);

	return [renderTargetTexture, frameBuffer, renderBuffer];
}

function bindData(gl,data){
	// Load data into the GPU buffer
	var bufferId = gl.createBuffer();
	gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );
	gl.bufferData( gl.ARRAY_BUFFER, flatten(data), gl.STATIC_DRAW );
	return bufferId;
}

function enableShaderAttribute(gl,bufferId,shaderAttribName,noElements,program){
	// Associate out shader variables with our data buffer
	gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
	var attributeLoc = gl.getAttribLocation(program, shaderAttribName);
	gl.vertexAttribPointer(attributeLoc, noElements, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(attributeLoc);
	return attributeLoc;
}


function enableShaderAttributeLoc(gl,bufferId,AttribLocation,noElements){
	// Associate out shader variables with our data buffer
	gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
	gl.vertexAttribPointer(AttribLocation, noElements, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(AttribLocation);
}


function disableShaderAttribute(gl,shaderAttribName,program){
	var attributeLoc = gl.getAttribLocation(program, shaderAttribName);
	gl.disableVertexAttribArray(attributeLoc);
}

function keyUpFunc(){
	if(event.keyCode == 87) // w
		camera.moveForward = false;
	if(event.keyCode == 65) // a
		camera.moveLeft = false;
	if(event.keyCode == 83) // s
		camera.moveBackward = false;
	if(event.keyCode == 68) // d
		camera.moveRight = false;
	if(event.keyCode == 16) // shift
		camera.speed = 1.0;
}

function keyDownFunc(){
	if(event.keyCode == 87) // w
		camera.moveForward = true;
	if(event.keyCode == 65) // a
		camera.moveLeft = true;
	if(event.keyCode == 83) // s
		camera.moveBackward = true;
	if(event.keyCode == 68) // d
		camera.moveRight = true;
	if(event.keyCode == 69){ // e
		if(showTexture)
			showTexture = false;
		else
			showTexture = true;
	}
	if(event.keyCode == 16) // shift
		camera.speed = 10.0;
}

function mouseMoveFunc(){
	var movementX = event.movementX ||
					event.mozMovementX ||
					event.webkitMovementX || 0;
	var movementY = event.movementY ||
					event.mozMovementY ||
					event.webkitMovementY || 0;
	camera.yaw += (movementX) * 0.07;
	camera.pitch += (movementY) * 0.07;
	if(camera.pitch>80)
		camera.pitch=80;
	if(camera.pitch<-80)
		camera.pitch=-80;

	mouseX = movementX-canvas.offsetLeft;
	mouseY = movementY-canvas.offsetTop;
	camera.update = true;
}

var projMat;
function resizeWindow(){ 
	var winWidth = window.innerWidth;
	var winHeight = window.innerHeight;
	if (canvas.width  != winWidth || canvas.height  != winHeight){
		canvas.width  = winWidth;
		canvas.height = winHeight;
		projMat = perspectivet(canvas.width / canvas.height);
	}
	gl.viewport(0, 0, winWidth, winHeight);
	return projMat;
}

function removeBuffer(program,AttribName,bufferId){
	gl.disableVertexAttribArray(gl.getAttribLocation(program, AttribName));
	gl.deleteBuffer(bufferId);
}

function perspectivet(ratio){
	var projMatrix = [	2.414213562373095/ratio, 0, 0, 0,
						0, 2.414213562373095, 0,       0,
						0, 0, -1.0000200002000021,    -1,
						0, 0, -0.2000020000200002,     0];
	var floats = new Float32Array(projMatrix);
	return floats;
}

function flatten(v){ // code from MV.js
	var n = v.length*v[0].length;
	var floats = new Float32Array(n);

	var idx = 0;
	for ( var i = 0; i < v.length; ++i ) {
		for ( var j = 0; j < v[i].length; ++j ) {
			floats[idx++] = v[i][j];
		}
	}

	return floats;
}