GPU N-Body Simulation using WebGL.

The program initially generates the particle positions, random mass, color, texture coordinates and velocity on the CPU which
is why it can take so long to startup with more than 100 000 particles. After that it uses the encodeShader to
store each particles data into a texture where position and mass on the left side and velocity on the right side.

Particle position and velocity calculation is done on the updateShader which reads from one texture, updates the position or velocity then writes it to the new one.
After that the textures are swapped when the render loops.

The method used to update the particles is currently using particle-particle/brute force method which is incredibly inefficient and may cause your browser to crash. 

I plan to update to the Barnes Hut algorithm or use Fast Multipole Method eventually.

https://yuh25.bitbucket.io/
