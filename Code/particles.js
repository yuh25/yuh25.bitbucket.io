var gl,renderProgram,encodeProgram,updateProgram,camera,projMatLoc,viewMatLoc;
var textRes = 128, numPoints = Math.pow(textRes,2)/2, showTexture = false;

window.onload = function init(){
	canvas = document.getElementById("gl-canvas");
	canvas.addEventListener("mousemove", mouseMoveFunc);
	window.addEventListener("keyup", keyUpFunc);
	window.addEventListener("keydown", keyDownFunc);

	canvas.requestPointerLock = canvas.requestPointerLock||
	/*lock the cursor*/			canvas.mozRequestPointerLock||
								canvas.webkitRequestPointerLock;
	canvas.onclick = function(){canvas.requestPointerLock();};

	gl = WebGLUtils.setupWebGL(canvas);
	ext = gl.getExtension("OES_texture_float");
	gl.getExtension("OES_texture_float_linear");
	if(!gl){alert("WebGL isn't available");}
	if(!ext){alert("WebGL Floating texture extension isn't available");}

	gl.enable(gl.DEPTH_TEST);
	gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	camera = new cameraClass([0, 0, 800]);

	// shader to store the buffers to the texture
	encodeProgram = initShaders(gl, "Shaders/encodeShader.vert",
										"Shaders/encodeShader.frag");

	// shader to calculate the physics and update texture
	updateProgram = initShaders(gl, "Shaders/updateShader.vert",
										"Shaders/updateShader.frag");
	// shader to render to the screen
	renderProgram = initShaders( gl, "Shaders/renderShader.vert", 
										"Shaders/renderShader.frag");

	// plane data use to draw the point data to the texture
	planePos = [[-1,1,0], [-1,0,0], [0,1,0], [0,0,0], [0,1,0], [-1,0,0]];
	planeTextCoord = [[0, 1], [0, 0], [1, 1], [1, 0], [1, 1], [0, 0]];

	planePosId = bindData(gl,planePos);
	textCoordId = bindData(gl,planeTextCoord);
	posLoc = enableShaderAttribute(gl, planePosId, "aPosition", 3, updateProgram);
	textCoordLoc = enableShaderAttribute(gl, textCoordId, "aTextCoord", 2, updateProgram);

	console.log('Generating Point Data', numPoints);
	data = generateParticleData();
	// [position, velocity, mass, pTextCoords, pColour];
	// Load paritcle data into the GPU buffers
	pTextCoordId = bindData(gl, data[3]);
	pColorId = bindData(gl, data[4]);
	colorLoc = enableShaderAttribute(gl, pColorId, "aColor", 4, renderProgram);
	coordLoc = enableShaderAttribute(gl, pTextCoordId, "aPTextCoord", 2, renderProgram);

	posBufferId = bindData(gl, data[0]);
	velBufferId = bindData(gl, data[1]);
	massBufferId = bindData(gl, data[2]);
	enableShaderAttribute(gl, posBufferId, "aPosition", 3, encodeProgram);
	enableShaderAttribute(gl, velBufferId, "aVelocity", 3, encodeProgram);
	enableShaderAttribute(gl, pTextCoordId, "aPTextCoord", 2, encodeProgram);
	enableShaderAttribute(gl, massBufferId, "aMass", 1, encodeProgram);

	// create the 'texture buffers'
	particleBuffers = [	generateTextureBuffer(textRes),
						generateTextureBuffer(textRes)];

	// set the resolution for creating the point locations
	gl.useProgram(renderProgram);
	gl.uniform1i(gl.getUniformLocation(renderProgram, "texture"), 0);
	projMatLoc = gl.getUniformLocation(renderProgram, "umProjMatrix");
	viewMatLoc = gl.getUniformLocation(renderProgram, "umViewMatrix");
	gl.useProgram(updateProgram);
	gl.uniform1i(gl.getUniformLocation(updateProgram, "texture"), 0);

	convertBuffersToTextures();
	data = null;
	textureBuf = true; // true - use 1st texture, 2nd renderBuffer
	console.log('Rendering');
	setInterval(render, 7); // 144hz -7ms
};



function convertBuffersToTextures(){
	gl.useProgram(encodeProgram);
	// set the texture resolution
	gl.uniform1f(gl.getUniformLocation(encodeProgram, "px"), 1.0 / textRes);
	gl.uniform1i(gl.getUniformLocation(encodeProgram, "texture"), 0);

	// render to the first buffer
	gl.bindFramebuffer(gl.FRAMEBUFFER,particleBuffers[0][1]);
	gl.bindRenderbuffer(gl.RENDERBUFFER,particleBuffers[0][2]);
	gl.viewport(0, 0, textRes, textRes);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	// initalise Texture
	gl.uniform1i(gl.getUniformLocation(encodeProgram, "side"), 0);
	gl.drawArrays(gl.POINTS, 0, numPoints);
	gl.uniform1i(gl.getUniformLocation(encodeProgram, "side"), 1);
	gl.drawArrays(gl.POINTS, 0, numPoints);

	// remove the buffers since we're using textures now.
	removeBuffer(encodeProgram, "aVelocity", velBufferId);
	removeBuffer(encodeProgram, "aPosition", posBufferId);
	removeBuffer(encodeProgram, "aMass", massBufferId);
}



function updateTexture(a,i){
	gl.useProgram(updateProgram);

	gl.bindTexture(gl.TEXTURE_2D, particleBuffers[a][0]);
	gl.viewport(0, 0, textRes, textRes);

	// set the render buffer and frame buffer to the other buffer
	gl.bindFramebuffer(gl.FRAMEBUFFER, particleBuffers[i][1]);
	gl.bindRenderbuffer(gl.RENDERBUFFER, particleBuffers[i][2]);

	// clear the framebuffer
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	enableShaderAttributeLoc(gl,planePosId, posLoc, 3);
	enableShaderAttributeLoc(gl,textCoordId, textCoordLoc, 2);
	gl.drawArrays(gl.TRIANGLES, 0, 6);

	//render the texture to the bottom left corner
	if(showTexture){
		gl.viewport(0, 0, 128, 128);
		gl.bindFramebuffer(gl.FRAMEBUFFER, null);
		gl.bindRenderbuffer(gl.RENDERBUFFER, null);
		gl.drawArrays(gl.TRIANGLES, 0, 6);
	}
}



function render(){
	camera.moveCamera();
	var i = textureBuf ? 1 : 0; // if texturebuf is true set to 1 else 0 read
	var a = textureBuf ? 0 : 1; // i: write to, a: read from
	gl.useProgram(renderProgram);
	// select the texture to decode the points from
	gl.bindTexture(gl.TEXTURE_2D, particleBuffers[a][0]);
	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	gl.bindRenderbuffer(gl.RENDERBUFFER, null);

	// send uniforms to the shader
	gl.uniformMatrix4fv(projMatLoc, gl.FALSE, resizeWindow());
	gl.uniformMatrix4fv(viewMatLoc, gl.FALSE, camera.calcMatrix());

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	enableShaderAttributeLoc(gl, pTextCoordId,coordLoc, 2);
	enableShaderAttributeLoc(gl, pColorId, colorLoc, 4 );
	gl.drawArrays(gl.POINTS, 0, numPoints);
	updateTexture(a,i);
	textureBuf = textureBuf ? false : true; // Change buffers to render from
}



function generateParticleData(){
	var radius = 100;
	var px = 1.0 / textRes;
	var halfRes = textRes / 2.0;
	var p = Math.pow(2,3);
	var pos = [numPoints], pColor = [numPoints], mass = [numPoints],
		vel = [numPoints], pCoords = [numPoints];

	for(var i = 0; i < numPoints; i++){
		var x,y,z;
		var t = Math.PI / 180 * (Math.floor(i / 360) * p);
		var s = Math.PI / 180 * i;

		x = radius * Math.cos(s) * Math.sin(t);
		y = radius * Math.sin(s) * Math.sin(t);
		z = radius * Math.cos(t);

		var textX = ((i / halfRes -Math.floor(i / halfRes) - 1 + px) + 1) / 2;
		var textY = (1 -(Math.floor(i / halfRes) / (numPoints / halfRes))) - px;
		pos[i] = [textX*1000-250, textY*500-250, 0];
		vel[i] = [pos[i][1] * 0.001, -pos[i][0] * 0.001, 0.0];
		pCoords[i] = [textX, textY];

		var r = 1.0 - Math.abs((i - numPoints) / numPoints);
		var g = 1.0 - Math.abs(i / numPoints);
		var b = 1.0 - Math.abs((i - numPoints / 2.0) / numPoints);
		pColor[i] = [r, g, b, 1.0];

		mass[i] = [Math.pow(Math.random(), Math.E * 20) * 20000.0 + 1.0];
	}
	return [pos, vel, mass, pCoords, pColor];
} // 196