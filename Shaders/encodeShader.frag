precision highp float;
varying vec4 vColor;
varying vec2 point;

void main(void){
	gl_FragColor = vColor;
}