//
// This shader converts the particle data points to a texture
//
precision highp float;
attribute vec3 aPosition;
attribute vec3 aVelocity;
attribute vec2 aPTextCoord;
attribute float aMass;

uniform int side;
uniform float px;

varying vec4 vColor;

void main(void){
	float x = aPTextCoord.x * 2.0;
	if(side == 0){ // Store Velocity and Mass Data
		vColor = vec4(aVelocity, aMass);
	} else { // Store Position Data and Mass Data
		x -= 1.0;
		vColor = vec4(aPosition, aMass);
	}
	gl_Position = vec4(x, aPTextCoord.y * 2.0 - 1.0 + px, 0.0, 1.0);
	gl_PointSize = 1.0;
}
