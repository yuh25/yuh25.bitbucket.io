precision highp float;
//
// This fragment shader reads the particles value from one texture, then either 
// updates its position or updates the velocity by calculating the force applied
// to it from every other particle. This is very costly (O(n^2)) hopefully will
// update to FMM.
//
varying vec2 vTextCoord;

uniform sampler2D texture;

const float res = 128.0;
float halfRes = 64.0;
const float numPoints = 8192.0;
float px = 1.0/res;

void main(void){
	// read the data for the current point (postion or velocity+mass Data)
	vec4 colourOut = texture2D(texture, vTextCoord);
	if(vTextCoord.x < 0.5){ // updating position - colourOut = position
		//read the data from the texture
		vec4 vel = texture2D(texture, vec2(vTextCoord.x + 0.5, vTextCoord.y));
		colourOut += vec4(vel.rgb, 1.0);
		colourOut.a = vel.a;
	} else { //Velocity calc - colourOut = velocity and mass
		vec4 pos = texture2D(texture, vec2(vTextCoord.x - 0.5, vTextCoord.y));
		vec3 velocity = colourOut.rgb;
		//Final Force applied to particle
		vec3 F = vec3(0.0);
		// calculate the force applied to the current particle from every other
		for(float i = 0.0; i < numPoints; i++){
			//Read the particle position from the texture
			float h = i / halfRes;
			float f = -floor(h);
			float x = (h + f + px) / 2.0;
			float y = f / (numPoints / halfRes) + 1.0 - px;
			vec4 iPosition = texture2D(texture, vec2(x, y));
			vec3 disp = iPosition.rgb - pos.rgb;
			float length = length(disp);
			if(length>1.0)
				F += (colourOut.a * iPosition.a * normalize(disp)) / length;
				// Code from https://github.com/asdvek/nbody/blob/master/main.py
		}
		velocity += (F/colourOut.a*0.00000002);
		colourOut = vec4(velocity,colourOut.a);
	}
	gl_FragColor = colourOut;
}
